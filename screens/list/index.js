import React from 'react'
import Ionicons from 'react-native-vector-icons/MaterialIcons';
import { View, ScrollView, Text, Image, TouchableHighlight } from 'react-native'
import ApiService from '../../services/api';
import Spinner from '../../components/spinner'
import Styles from './style'

export default class ListScreen extends React.Component {
    constructor(props) {
        super(props)

        this.Api = new ApiService()

        this.state = {
            loading: true,
            data: false,
        }
    }

    componentDidMount() {
        this.loadData()
    }

    async loadData() {
        this.setState({ loading: true })
        let req = await this.Api.get('blog/public/api/posts', {})
        let data = req.data
        this.setState({ data, loading: false })
    }

    getList() {
        const { data } = this.state

        let list = data.map((post, i) => {
            return (
                <TouchableHighlight key={i} onPress={() => {
                    this.props.navigation.navigate('Create', Object.assign(post, { parent: this }), 'Novo Post')
                }}>
                    <View style={Styles.listView}>
                        <Text style={Styles.listItem}>{post.title}</Text>
                        <Ionicons name={`chevron-right`} size={30} color={`#c834b9`} style={Styles.listIcon} />
                    </View>
                </TouchableHighlight>)
        })

        return list
    }

    render() {
        const { loading } = this.state

        return (
            loading ? (
                <Spinner />
            ) : (<ScrollView style={Styles.container}>
                {this.getList()}
            </ScrollView>)
        )
    }
}