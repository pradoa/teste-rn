export default style = {
    listView: {
        borderColor: '#c834b9',
        borderWidth: 1,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 10,
        paddingBottom: 10,
        flex: 1,
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    listItem: {
        fontSize: 24,
        fontWeight: '700',
        display: 'flex',
        alignItems: 'flex-start'
    },
    listIcon: {
        alignItems: 'flex-end',
        textAlign: 'right',
        width: '10%',
        display: 'flex',
        alignItems: 'flex-end'
    },
    container: { marginTop: 22 },


}