import React from 'react'
import Ionicons from 'react-native-vector-icons/MaterialIcons';
import { View, ScrollView, Text, TextInput, TouchableHighlight, TouchableWithoutFeedback, Keyboard } from 'react-native'
import ApiService from '../../services/api';
import Spinner from '../../components/spinner';

import Styles from './style'

export default class Screen extends React.Component {
    constructor(props) {
        super(props)

        const { params } = this.props.navigation.state
        console.log(params)

        this.Api = new ApiService()

        this.state = {
            loading: false,
            data: false,
            parent: params.parent ? params.parent : false,
            id: params.id ? params.id : false,
            title: params.title ? params.title : false,
            description: params.description ? params.description : false,
        }
    }

    componentWillReceiveProps(props) {
        console.log(props)
        this.clearForm()
    }

    componentDidMount() {
        const { id } = this.state
    }

    clearForm() {
        this.titleInput.clear()
        this.descriptionInput.clear()
    }

    hideKeyboard() {
        Keyboard.dismiss()
    }

    updateData(key, val) {
        this.setState({
            [key]: val
        })
    }

    async savePost() {
        const { title, description, id, parent } = this.state

        if (title && description) {
            if (id) {
                let req = await this.Api.put(`blog/public/api/posts/${id}`, {
                    title,
                    description
                })

                console.log(req)

                if (req.data && req.data.id) {
                    this.clearForm()
                    this.props.navigation.pop()
                    parent.loadData()
                }
            } else {
                let req = await this.Api.post('blog/public/api/posts', {
                    title,
                    description
                })

                if (req.data && req.data.status) {
                    this.clearForm()
                    this.props.navigation.pop()
                    parent.loadData()
                }
            }
        }
    }

    async deletePost() {
        const { title, description, id, parent } = this.state

        if (id) {
            let req = await this.Api.delete(`blog/public/api/posts/${id}`, {})
            console.log(req)
            if (req.data && req.data.status) {
                this.clearForm()
                this.props.navigation.pop()
                parent.loadData()
            }
        }
    }

    getForm() {
        const { id, title, description } = this.state

        return (
            <TouchableWithoutFeedback style={Styles.form} onPress={() => { this.hideKeyboard() }}>
                <View>
                    <Text style={Styles.titleLabel}>Título ({id})</Text>
                    <TextInput style={Styles.titleInput} ref={(input) => { this.titleInput = input }} defaultValue={title} onChangeText={(text) => {
                        this.updateData('title', text)
                    }} />

                    <Text style={Styles.descriptionLabel}>Descrição</Text>
                    <TextInput style={Styles.descriptionInput} ref={(input) => { this.descriptionInput = input }} defaultValue={description} numberOfLines={5} multiline={true} onChangeText={(text) => {
                        this.updateData('description', text)
                    }} />


                    <TouchableHighlight style={id ? Styles.deleteBtn : Styles.deleteBtnDisabled}>
                        <Ionicons.Button name={`close`} size={40} color={`#fff`} backgroundColor={'transparent'} style={Styles.btnIcon} onPress={() => { this.deletePost() }}>
                            <Text style={Styles.deleteBtnTitle}>Excluir post</Text>
                        </Ionicons.Button>
                    </TouchableHighlight>

                    <TouchableHighlight style={Styles.saveBtn} onPress={() => { }}>
                        <Ionicons.Button name={`done`} size={40} color={`#fff`} backgroundColor={'transparent'} style={Styles.btnIcon} onPress={() => { this.savePost() }}>
                            <Text style={Styles.saveBtnTitle}>
                                Salvar post
                        </Text>
                        </Ionicons.Button>
                    </TouchableHighlight>
                </View>
            </TouchableWithoutFeedback >
        )
    }

    render() {
        const { loading, id } = this.state

        return (
            loading ? (
                <Spinner />
            ) : (<View style={Styles.container}>
                {this.getForm()}
            </View>)
        )
    }
}