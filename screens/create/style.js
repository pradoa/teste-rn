export default style = {
    container: { marginTop: 20 },
    form: {
        backgroudColor: '#777'
    },
    titleLabel: {
        textAlign: 'center',
        fontSize: 24,
        color: '#c834b9',
        marginTop: 20,
    },
    titleInput: {
        borderRadius: 12,
        borderWidth: 1,
        borderColor: '#ccc',
        marginLeft: 10,
        marginRight: 10,
        fontSize: 24,
        paddingTop: 8,
        paddingBottom: 8,
        paddingLeft: 15,
        paddingRight: 15,
        marginTop: 10
    },
    descriptionLabel: {
        textAlign: 'center',
        fontSize: 24,
        color: '#c834b9',
        marginTop: 20,
    },
    descriptionInput: {
        height: 200,
        borderRadius: 12,
        borderWidth: 1,
        borderColor: '#ccc',
        marginLeft: 10,
        marginRight: 10,
        fontSize: 24,
        paddingTop: 8,
        paddingBottom: 8,
        paddingLeft: 15,
        paddingRight: 15,
        marginTop: 10,
        marginBottom: 42,
    },

    deleteBtn: {
        backgroundColor: '#fa393b',
        width: '100%',
    },
    deleteBtnDisabled: {
        backgroundColor: '#fa393b',
        width: '100%',
        opacity: 0.5
    },
    deleteBtnTitle: {
        fontSize: 24,
        fontWeight: '700',
        color: '#fff',
        flexDirection: 'row',
        textAlign: 'center'
    },

    saveBtn: {
        backgroundColor: '#314bee',
        width: '100%',
    },
    saveBtnTitle: {
        fontSize: 24,
        fontWeight: '700',
        color: '#fff',
        flexDirection: 'row',
        textAlign: 'center'
    },

    btnIcon: {
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        padding: 13,
    }
}