/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';

import Navigator from './components/navigator'

export default class App extends Component<Props> {
  render() {
    return (
      <Navigator />
    );
  }
}
