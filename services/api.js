import { Observable } from 'rxjs';
import { API_URL, USER_INFO } from '../settings';
import axios from 'axios';

export class ApiService {

    async put(url, data) {
        try {
            let udata = USER_INFO
            const response = await axios.put(`${API_URL}/${url}`, data, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': udata && udata.token ? udata.token : false
                }
            })
            return response;
        } catch (error) {
            return error;
        }
    }

    async delete(url, data) {
        try {
            let udata = USER_INFO
            const response = await axios.delete(`${API_URL}/${url}`, data, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': udata && udata.token ? udata.token : false
                }
            })
            return response;
        } catch (error) {
            return error;
        }
    }

    async post(url, data) {
        try {
            let udata = USER_INFO
            const response = await axios.post(`${API_URL}/${url}`, data, {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': udata && udata.token ? udata.token : false
                }
            })
            return response;
        } catch (error) {
            return error;
        }
    }

    async get(url, data) {
        try {
            let udata = USER_INFO
            const response = await axios.get(`${API_URL}/${url}`, {
                params: data,
                headers: {
                    'x-access-token': udata && udata.token ? udata.token : false
                }
            })
            return response;
        } catch (error) {
            return false
        }
    }

    checkResponse(response) {
        if (response.ok) {
            return response;
        }

        throw response;
    }

    connection() {
        return this._connection$.asObservable();
    }

}

export default ApiService