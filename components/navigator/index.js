// You can import Ionicons from @expo/vector-icons if you use Expo or
// react-native-vector-icons/Ionicons otherwise.
import Ionicons from 'react-native-vector-icons/MaterialIcons';
import { createBottomTabNavigator, createAppContainer, createStackNavigator, NavigationActions, StackNavigator } from 'react-navigation';
import React from 'react'
import { Text, Platform, TouchableHighlight } from 'react-native'

import ListScreen from '../../screens/list'
import CreateEditScreen from '../../screens/create'

const AddButton = ({ nav }) => {
    return (
        <TouchableHighlight onPress={() => {
            nav.navigate(Main, { id: 0 }, 'Create')
        }} underlayColor={'transparent'}>
            <Ionicons name="add-circle-outline" size={25} color={'#ee4145'} />
        </TouchableHighlight>
    )
}

const Main = createStackNavigator({
    Main: {
        screen: ListScreen,
        navigationOptions: {
            tabBarVisible: false,
        }
    },
    Create: {
        screen: props => <CreateEditScreen {...props} />,
        navigationOptions: ({ navigation, screenProps }) => ({
            headerLeft: <Ionicons name={Platform.OS === 'ios' ? `chevron-left` : `arrow-back`} size={30} color={`#c834b9`} onPress={() => {
                navigation.pop()
            }} />,
            tabBarVisible: false,
        })
    }
})

const Nav = createBottomTabNavigator(
    {
        'Listar Todos': Main,
        'Novo Post': {
            screen: CreateEditScreen,
            navigationOptions: ({ navigation }) => ({
                headerLeft: <Ionicons name={Platform.OS === 'ios' ? `chevron-left` : `arrow-back`} size={30} color={`#c834b9`} onPress={() => {
                    navigation.pop()
                }} />,
            })
        },
    },
    {
        defaultNavigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, horizontal, tintColor }) => {
                const { routeName } = navigation.state;
                let iconName;

                switch (routeName) {
                    case 'Listar Todos': iconName = `reorder`; break;
                    case 'Novo Post': iconName = `add-circle-outline`; break;
                }

                // You can return any component that you like here! We usually use an
                // icon component from react-native-vector-icons
                return <Ionicons name={iconName} size={horizontal ? 20 : 25} color={'#ee4145'} />;
            },
        }),
        tabBarOptions: {
            activeTintColor: 'tomato',
            inactiveTintColor: 'tomato',
        },
        backBehavior: 'Listar Todos',
    }
);

export default createAppContainer(Nav)