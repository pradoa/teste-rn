import React from 'react'
import { Image } from 'react-native'
import Style from './style'

export default class Spinner extends React.Component {
    render() {
        return (
            <Image source={require('../../images/loading.gif')} style={Style.spinner} />
        )
    }
}