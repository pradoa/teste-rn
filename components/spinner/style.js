import { Dimensions } from 'react-native'

const { height, width } = Dimensions.get('window');
const ml = ((width / 2) - 25)

export default Styles = {
    spinner: {
        width: 50,
        height: 50,
        display: 'flex',
        alignContent: 'center',
        justifyContent: 'center',
        marginLeft: ml,
        marginTop: 50
    }
}